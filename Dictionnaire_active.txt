Libell�	code	type et longueur	regle	commentaire	
matricule	ID	int	incr�mentation auto		
nom et pr�nom du collaborateur	name	varchar(100)			
sexe	gender	char(1)	F ou M	Le sexe F correspond � un code civil Mme ou Mlle Le sexe M correspond obligatoirement au code civil M.	
�tat civil		varchar(3)	MR,Mme,Mle		
adresse 1		varchar(100)			
adresse 2		varchar(100)			
ville		varchar(50)			
code postal		char(5)			
t�l�phone		char(10)			
type de contrat		char(3)	CDD,CDI,STA  Passages possibles de : S (stagiaire) � D (CDD) ou I (CDI), D (CDD) � I (CDI).	faire une table, c'est restrictif et ca peut �voluer	
salaire collaborateur		int	en centimes		
date d'embauche collaborateur		date		soit la date du premier contrat CDD ou CDI	
date de fin de contrat		date	NULL	attention on peut avoir plusieurs contrats ou avenant!!!!	
statut collaborateur		varchar(100)		diff�rent de la fonction (statut=contrat, fonction=projet)? Statut = ing�nieur d'�tude	
id fonction		int	incr�mentation auto		
fonction collaborateur		varchar(50)		1 fonction/collaborateur, peut varier d'un projet � l'autre	
nom de projet abr�g�		varchar(10)			
nom de projet long		varchar(50)			
code projet		char(4)	unique,compos� pour les deux premiers par l?ann�e en cours et sur les deux derniers par un num�ro s�quentiel +1 � partir du dernier connu.		
type de projet		char(1)	F : forfait, R : r�gie, A: assistance		
secteur d'activit� du projet		varchar(100)		(Gestion Commercial, RH, Production, Achat, ?)	
cycle de vie du projet		varchar(30)		(complet, �tudes de l?existant, d�veloppement, ?),	
charge globale estim�e du projet		int	en heure	calcul�	
date pr�visionnelle de d�but		date	<= date pr�visio fin		
date pr�visionnelle de fin		date			
Commentaire estimation		varchar(255)			
date effective de d�but		date	NULL		
date effective de fin		date	NULL		
Commentaire technique		varchar(255)		doit faire l'objet d'une entit� sp�cifique,- Langages, plate-forme, outils de d�veloppement,?	
taille maximum de l'�quipe		int		nombre de personnes	
n� de lot 		char(6)	4 chiffres du num�ro de projet + plus un num�ro s�quentiel sur deux positions		
charge  estim�e		int	en heure	par �tapes du processus	
charge r�elles de production		int	en heure,NULL	par �tapes du processus	
charge r�elles de validation		int	en heure,NULL	par �tapes du processus	
Collaborateurs associ�s				avec sa fonction sur le projet, date d?intervention, type d?intervention	
id activit�		int			
nom activit�		varchar(100)			
id		int			
label		varchar(255)	NULL	p�le-m�le d'informations techniques	
identifiant doc projet		int	s�quentiel		
titre doc projet		varchar(100)			
r�sum� doc projet		varchar(255)			
auteur doc projet		varchar(100)		est un collaborateur interne, peuvent-ils �tre plusieurs? Par ex : client cahier des charges	
date de diffusion		date			
date de d�but intervention		datetime			
date de fin intervention		datetime	NULL	on doit pouvoir calclu� une charge en heure	
num�ro d'intervention			unique		
n� client		int	num�rique		
raison sociale		varchar(100)			
nom contact projet		varchar(100)			
fonction contact projet		varchar(100)			
coordonn�es soci�t�				fautil les m�me champs que pour le collaborateur	
t�l�phone		char(10)		et pour l'international ?	
type de soci�t�		tinyInt	"1 : Priv�
2: Public"		
domaine d'activit�		varchar(100)		ex: agro	
nature		enum	� principale �, � secondaire �, � ancienne �).	nature � ancienne � �tant un �tat de quasi-archivage	
chiffre affaire		int	CA/eff <= 1 000 000?	en millier d'?	
effectif de la soci�t�			???	un chiffre d?affaire sup�rieur � 1 M? par personne est consid�r� comme erron�.	
commentaires g�n�raux		varchar(255)	NULL		
commentaire sp�cifique		varchar(255)	NULL	est-ce les commentaires li� au projet? 	
