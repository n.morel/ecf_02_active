-- On veut lister les interventions des collaborateurs sur un projet entre deux dates.

DROP PROCEDURE IF EXISTS Collaborator_Intervention;

DELIMITER $$

create Procedure Collaborator_Intervention(in param1 date,in param2 date, param3 int)
COMMENT "liste des interventions"
reads sql data
BEGIN
	declare dateMin, dateMax date;
    declare param_project int;
    set dateMin = param1, dateMax = param2, param_project = param3;
    
	select C.fullName,F.label,I.startDate,I.endDate, A.label 
    from Intervention I
	inner join Collaborator C on I.MATRICULE = C.MATRICULE
    inner join FunctionType F on I.idFunctionType = F.idFunctionType
    inner join Activity A on I.idActivity = A.idactivity
	inner join Project P on C.matricule = P.matricule
	where P.idProject = param_project 
    and I.endDate >= dateMin 
    and I.startDate <= dateMax;	  
      
END$$