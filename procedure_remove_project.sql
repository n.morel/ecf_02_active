/* Supprimer de la base de données les projets qui sont terminés et qui n’ont pas eut de charges (étapes) associées.
   ne supprimer que s'il n'y a pas d'interventions ou autres associations          */
DROP PROCEDURE IF EXISTS remove_project;

DELIMITER //

create Procedure remove_project ()
COMMENT "suppression des projets sans charges"
reads sql data
BEGIN
	delete project, step from project
    inner join step  on project.idProject = Step.idProject
    left join  step S on project.idProject = S.idProject
    left join Intervention I   on step.idStep = I.IDSTEP
    left join `comment` C on  I.idintervention = C.idintervention
    left join `comment` on  step.idstep = `comment`.IDSTEP
    left join projectlibrary  on project.IDPROJECT = projectlibrary.IDPROJECT
    left join projecttechnology on project.IDPROJECT = projecttechnology.IDPROJECT
    where (step.productionLoad is null or step.productionLoad =0)
    and (Step.validationLoad is null or step.validationLoad =0)
    ;
     
END//
delimiter ;