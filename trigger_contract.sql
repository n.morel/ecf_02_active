/* Vérifier la cohérence du code statut,
Passages possibles de :
- S (stagiaire) à D (CDD) ou I (CDI),
- D (CDD) à I (CDI).*/

drop  TRIGGER  if exists `Contract_before_insert`;


delimiter //
CREATE  TRIGGER `Contract_before_insert` BEFORE insert ON `Contract` FOR EACH ROW
BEGIN
	declare lastContract char(3) default null;
    declare lastEndDate date default null;
    
	select  C.idct, C.enddate 
    into lastContract, lastEndDate
	from contract C
	inner join (select 
    max(enddate) as maxDate
	from contract  where matricule = new.matricule ) D
	on C.enddate = D.maxDate ;
    
    if lastcontract like 'cdi' and (new.idct like 'sta' or new.idct like 'cdd')
    then signal sqlstate "45000"
    set message_text = "inconsistent data"
    ;
    elseif  lastcontract like 'cdd' and new.idct like 'sta' 
    then signal sqlstate "45000"
    set message_text = "inconsistent data"
    ;
    elseif  lastEndDate > new.startdate
    then signal sqlstate "45000"
    set message_text = "can't begin at this date"
    ;
    end if;	
  
END//

delimiter ;