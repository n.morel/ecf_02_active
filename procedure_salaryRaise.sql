drop PROCEDURE  if exists `salary_raise`;
delimiter $$
CREATE PROCEDURE `salary_raise`()
    READS SQL DATA
    COMMENT 'raise after 5 years of service '
BEGIN

declare counter int default 1;
declare duration int default 0;


	 WHILE counter <= (select count(distinct matricule) from contract) do
		select C.matricule, C.STARTDATE from contract C
		inner join 
		/*  on fait la diffèrence la durée du dernier contrat (la durée du contrat en cours -soit la durée minimum entre aujourd'hui et startdate:
			min(datediff(curdate(),startdate))
			on ajoute la somme des durées des contrat terminés: sum(datediff(enddate,startdate)
			*/
		(select matricule, min(datediff(curdate(),startdate)) + sum(datediff(enddate,startdate)) as duration
		from contract
		where MATRICULE = counter) D
		on c.MATRICULE = d.MATRICULE
		where C.endDate is null
        group by C.MATRICULE
		into duration
		;
				
		if duration > 1825 then
		update contract
		set salary = salary * 1.05 ;
		end if;
        
        SET counter = counter + 1;
	END WHILE;
     
END$$

delimiter ;