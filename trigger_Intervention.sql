drop  TRIGGER  if exists `intervention_BEFORE_INSERT`;
drop  TRIGGER  if exists `intervention_BEFORE_UPDATE`;

delimiter //
CREATE  TRIGGER `intervention_BEFORE_INSERT` BEFORE INSERT ON `intervention` FOR EACH ROW
BEGIN
	if new.matricule is not null 
    and  new.idcontact is not null 
	then signal sqlstate "45000"
    set message_text = "IDcontact or matricule must be null"
    ;    
    elseif  new.idcontact is null
    and new.matricule is null
    then signal sqlstate "45000"
    set message_text = "IDcontact and matricule can't be null"
    ;
    elseif new.matricule is not null
    and new.idfunctiontype is null
    then signal sqlstate "45000"
    set message_text = "IDfunctiontype can't be null"
    ;
    end if;
END//


CREATE  TRIGGER `intervention_BEFORE_update` BEFORE update ON `intervention` FOR EACH ROW
BEGIN
	if new.matricule is not null 
    and  new.idcontact  is not null 
	then signal sqlstate "45000"
    set message_text = "IDcontact or matricule must be null"
    ;    
    elseif  new.matricule = null
    and new.idcontact = null
    then signal sqlstate "45000"
    set message_text = "IDcontact and matricule can't be null"
    ;
    elseif new.matricule is not null
    and new.idfunctiontype is null
    then signal sqlstate "45000"
    set message_text = "IDfunctiontype can't be null"
    ;
    end if;
END//
delimiter ;