drop  TRIGGER  if exists `Projet_BEFORE_insert`;
drop  TRIGGER  if exists `Projet_BEFORE_update`;
drop  TRIGGER  if exists `Projet_BEFORE_delete`;

delimiter //
CREATE  TRIGGER `Projet_BEFORE_insert` BEFORE insert ON `project` FOR EACH ROW
BEGIN
	if new.endDate < new.startDate
	then signal sqlstate "45000"
    set message_text = "endDate can't be smaller than StartDate"
    ;  
    end if;
    set new.code = new_project_code();
END//

CREATE  TRIGGER `Projet_BEFORE_update` BEFORE update ON `project` FOR EACH ROW
BEGIN
	if new.endDate < new.startDate
	then signal sqlstate "45000"
    set message_text = "endDate can't be smaller than StartDate"
    ;    
	end if;
    set new.code = old.code;
END//

CREATE  TRIGGER `Projet_BEFORE_DELETE` BEFORE DELETE ON `project` FOR EACH ROW
BEGIN
	if datediff(curdate(),old.endDate) <61
	then signal sqlstate "45000"
    set message_text = "can't be delete earlier than 2 months"
    ;    
	end if;
END//


drop function if exists new_project_code//
CREATE DEFINER=`root`@`localhost` FUNCTION `new_project_code`() 
RETURNS char(4) 
-- CHARSET utf8
READS SQL DATA
COMMENT 'create a project code'
begin
	declare codemax char(4);
	select max(code) from project into codemax ;
	
	if substring(codemax,1,2) like substring(year(current_date()),3) then
		set codeMax = codeMax +1;
	else
		set codeMax = (substring(year(current_date()),3) *100) + 1; 
		
	end if;
	return codeMax; 
end//
delimiter ;