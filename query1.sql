-- On souhaite obtenir par secteur d’activité la moyenne des charges estimées des projets..

select sector,sum(LOADESTIMATION) from Step S
inner join project P on S.idProject = P.IDPROJECT
group by sector;

