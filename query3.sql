-- On souhaite obtenir à la date du jour la liste des projets en cours, par secteur d’activité. 
-- Préciser le nombre de collaborateurs associés aux projets et ceci par fonction



Select distinct  P.idProject, P.sector, F.label, c.matricule  from Project P
inner join Step S on P.IDPROJECT = S.IDPROJECT
inner join  Intervention I on S.IDSTEP = I.IDSTEP
inner join Collaborator C on I.MATRICULE = C.MATRICULE
inner join FunctionType F on I.idFunctionType = F.idFunctionType
where P.endDate is null
-- group by
order by sector;

-- comment compter les matricules? count?, group by?