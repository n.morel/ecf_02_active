drop function if exists new_step_batchNumber;

delimiter $$
CREATE DEFINER=`root`@`localhost` FUNCTION `new_step_batchNumber`(param int) 
RETURNS char(6) 
-- CHARSET utf8
READS SQL DATA
COMMENT 'create a step batchNumber'
begin
	declare code_project char(4);
    declare nb_step char(2);
    declare id_project int default 0;
    set id_project = param;
    
	select `code` as code_project from project    
    where idProject = id_project
    into code_project ;
	    
	select LPAD(count(idproject)+1,2,'0') as nb_step  from Step
    where idproject = id_project
    into nb_step
    ;
	return  concat(code_project,nb_step);
    
end$$

DROP TRIGGER IF EXISTS `active`.`step_BEFORE_INSERT`$$

CREATE DEFINER = CURRENT_USER TRIGGER `active`.`step_BEFORE_INSERT` BEFORE INSERT ON `step` FOR EACH ROW
BEGIN
	set new.batchNumber = new_step_batchNumber(new.idProject);
END$$
DELIMITER ;