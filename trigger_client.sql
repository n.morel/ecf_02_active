/*Vérifier la cohérence du chiffre d’affaire du client,
 si supérieur à 1 million d’euros par personne la valeur du CA est erronée.*/
 
 drop  TRIGGER  if exists `Client_before_insert`;
 drop  TRIGGER  if exists `Client_before_update`;
 
 delimiter //
CREATE  TRIGGER `Client_before_insert` BEFORE insert ON `client` FOR EACH ROW
BEGIN
	if new.revenue /new.workforce  > 1000
	then signal sqlstate "45000"
    set message_text = " inconsistent data"
    ;    
	end if;
END//

CREATE  TRIGGER `Client_before_update` BEFORE update ON `client` FOR EACH ROW
BEGIN
	if new.revenue /new.workforce  > 1000
	then signal sqlstate "45000"
    set message_text = " inconsistent data"
    ;    
	end if;
END//
delimiter ;