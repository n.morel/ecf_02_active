/* On souhaite obtenir la liste des projets (libellé court) 
sur lesquels un collaborateur est intervenu. 
Préciser également sa fonction dans les projets.
*/

select C.fullName, F.label, P.idProject, P.shortName from Project P
inner join Step S on P.IDPROJECT = S.IDPROJECT
inner join  Intervention I on S.IDSTEP = I.IDSTEP
inner join Collaborator C on I.MATRICULE = C.MATRICULE
inner join FunctionType F on I.idFunctionType = F.idFunctionType
group by P.idProject, label
order by C.fullName, P.idproject
;
-- doit-on inclure le chef de projet?