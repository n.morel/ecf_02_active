/*==============================================================*/
/* Nom de SGBD :  MySQL 5.0                                     */
/* Date de création :  12/02/2020 14:09:37                      */
/*==============================================================*/

-- create schema Active;
use Active;
set foreign_key_checks = 0;

drop table if exists Acl;
drop table if exists Activity;
drop table if exists `Client`;
drop table if exists Collaborator;
drop table if exists `Comment`;
drop table if exists Contact;
drop table if exists ContactClient;
drop table if exists Contract;
drop table if exists ContractType;
drop table if exists DefaultFunction;
drop table if exists Functiontype;
drop table if exists Intervention;
drop table if exists Permission;
drop table if exists Project;
drop table if exists ProjectLibrary;
drop table if exists ProjectTechnology;
drop table if exists `Resource`;
drop table if exists Rule;
drop table if exists `Status`;
drop table if exists Step;
drop table if exists StepType;
drop table if exists Technology;

set foreign_key_checks = 1;

/*==============================================================*/
/* Table : Rule                                                 */
/*==============================================================*/
create table Rule
(
   IDRULE               int not null auto_increment,
   `LABEL`              varchar(50) not null,
   primary key (IDRULE)
);

/*==============================================================*/
/* Table : Permission                                           */
/*==============================================================*/
create table Permission
(
   IDPERMISSION         int not null auto_increment,
   `KEY`                  varchar(30) not null,
   `DESCRIPTION`          varchar(200),
   primary key (IDPERMISSION)
);

/*==============================================================*/
/* Table : Acl                                                  */
/*==============================================================*/
create table Acl
(
   IDRULE               int not null,
   IDPERMISSION         int not null,
   primary key (IDRULE, IDPERMISSION),
   constraint FK_ACL foreign key (IDRULE)
      references Rule (IDRULE) on delete restrict on update restrict,
   constraint FK_ACL2 foreign key (IDPERMISSION)
      references Permission (IDPERMISSION) on delete restrict on update restrict
);

/*==============================================================*/
/* Table : Activity                                             */
/*==============================================================*/
create table Activity
(
   IDACTIVITY           int not null auto_increment,
   `LABEL`              varchar(50) not null,
   primary key (IDACTIVITY)
);

/*==============================================================*/
/* Table : Client                                               */
/*==============================================================*/
create table `Client`
(
   IDCLIENT             int not null auto_increment,
   COMPANYNAME          varchar(100) not null,
   ADRESS               varchar(100) not null,
   POSTCODE             char(5) not null,
   CITY                 varchar(50) not null,
   PHONE                varchar(14) not null,
   `TYPE`               enum ('Privé','Public') not null,
   AREA                 varchar(100) not null,
   NATURE               varchar(50) not null,
   REVENUE              int not null,
   WORKFORCE            int not null,
   primary key (IDCLIENT)
);

/*==============================================================*/
/* Table : Collaborator                                         */
/*==============================================================*/
create table Collaborator
(
   MATRICULE            int not null auto_increment,
   IDRULE               int,
   FULLNAME             varchar(100) not null,
   GENDER               enum ('H','F') not null,
   CIVILSTATUS          enum ('M','Mme','Mle') not null,
   ADRESS1              varchar(100) not null,
   ADRESS2              varchar(100) ,
   POSTALCODE           char(5) not null,
   CITY                 varchar(50) not null,
   PHONENUMBER          char(10) not null,
   USERNAME             varchar(30),
   `PASSWORD`            char(8),
   primary key (MATRICULE),
   constraint FK_HAVERULE foreign key (IDRULE)
      references Rule (IDRULE) on delete restrict on update restrict
);

/*==============================================================*/
/* Table : Project                                              */
/*==============================================================*/
create table Project
(
   IDPROJECT            int not null auto_increment,
   IDCLIENT             int not null,
   MATRICULE            int not null,
   CODE                 char(4) not null comment 'unique,compose pour les deux premiers par l’annee en cours et sur les deux derniers par un numero sequentiel +1 a partir du dernier connu.',
   SHORTNAME            varchar(10) not null,
   FULLNAME             varchar(100) not null,
   TYPE                 enum ('F','R','A') not null,
   SECTOR               varchar(100) not null,
   EXPECTEDSTARTDATE    date not null ,
   EXPECTEDENDDATE      date not null ,
   STARTDATE            date,
   ENDDATE              date,
   TEAMSIZE             int not null,
   primary key (IDPROJECT),
   constraint FK_MANAGE foreign key (MATRICULE)
      references Collaborator (MATRICULE) on delete restrict on update restrict,
   constraint FK_ORDER foreign key (IDCLIENT)
      references Client (IDCLIENT) on delete restrict on update restrict,
   check (EXPECTEDSTARTDATE  < EXPECTEDENDDATE),
   check (startDate < endDate)
);

/*==============================================================*/
/* Table : StepType                                             */
/*==============================================================*/
create table StepType
(
   IDSTEPTYPE           int not null auto_increment,
   `LABEL`              varchar(50) not null,
   primary key (IDSTEPTYPE)
);

/*==============================================================*/
/* Table : Step                                                 */
/*==============================================================*/
create table Step
(
   IDSTEP               int not null auto_increment,
   IDPROJECT            int not null,
   IDSTEPTYPE           int not null,
   BATCHNUMBER          char(6) not null comment '4 chiffres du numero de projet + plus un numero sequentiel sur deux positions',
   LOADESTIMATION       int not null,
   PRODUCTIONLOAD       int,
   VALIDATIONLOAD       int,
   primary key (IDSTEP),
   constraint FK_PROJECTSTEP foreign key (IDPROJECT)
      references Project (IDPROJECT) on delete restrict on update restrict,
   constraint FK_LIFETIME foreign key (IDSTEPTYPE)
      references StepType (IDSTEPTYPE) on delete restrict on update restrict
);

/*==============================================================*/
/* Table : Functiontype                                         */
/*==============================================================*/
create table Functiontype
(
   IDFUNCTIONTYPE       int not null auto_increment,
   `LABEL`              varchar(50) not null,
   primary key (IDFUNCTIONTYPE)
);

/*==============================================================*/
/* Table : Contact                                              */
/*==============================================================*/
create table Contact
(
   IDCONTACT            int not null auto_increment,
   FULLNAME             varchar(100) not null,
   `FUNCTION`             varchar(100) not null,
   PHONE                char(10) not null,
   primary key (IDCONTACT)
);

/*==============================================================*/
/* Table : Intervention                                         */
/*==============================================================*/
create table Intervention
(
   IDINTERVENTION       int not null auto_increment,
   IDACTIVITY           int not null,
   IDCONTACT            int,
   IDFUNCTIONTYPE       int,
   IDSTEP               int not null,
   MATRICULE            int,
   STARTDATE            date,
   ENDDATE              date,
   VOLUME               int not null,
   primary key (IDINTERVENTION),
   constraint FK_STEP foreign key (IDSTEP)
      references Step (IDSTEP) on delete restrict on update restrict,
   constraint FK_TEMPORARYFUNCTION foreign key (IDFUNCTIONTYPE)
      references Functiontype (IDFUNCTIONTYPE) on delete restrict on update restrict,
   constraint FK_ACT foreign key (MATRICULE)
      references Collaborator (MATRICULE) on delete restrict on update restrict,
   constraint FK_ACTIVITY foreign key (IDACTIVITY)
      references Activity (IDACTIVITY) on delete restrict on update restrict,
   constraint FK_INTERVENE foreign key (IDCONTACT)
      references Contact (IDCONTACT) on delete restrict on update restrict,
	check (startDate < endDate));

/*==============================================================*/
/* Table : Comment                                              */
/*==============================================================*/
create table `Comment`
(
   IDCOMMENT            int not null auto_increment,
   MATRICULE            int not null,
   IDCLIENT             int,
   IDSTEP               int,
   IDPROJECT            int,
   IDINTERVENTION       int,
   TEXT                 varchar(255) not null,
   primary key (IDCOMMENT),
   constraint FK_INTERVENTIONCOMMENT foreign key (IDINTERVENTION)
      references Intervention (IDINTERVENTION) on delete restrict on update restrict,
   constraint FK_STEPCOMMENT foreign key (IDSTEP)
      references Step (IDSTEP) on delete restrict on update restrict,
   constraint FK_PROJECTCOMMENT foreign key (IDPROJECT)
      references Project (IDPROJECT) on delete restrict on update restrict,
   constraint FK_CLIENTCOMMENT foreign key (IDCLIENT)
      references Client (IDCLIENT) on delete restrict on update restrict,
   constraint FK_COMMENTAUTHOR foreign key (MATRICULE)
      references Collaborator (MATRICULE) on delete restrict on update restrict
);

/*==============================================================*/
/* Table : ContactClient                                        */
/*==============================================================*/
create table ContactClient
(
   IDCLIENT             int not null auto_increment,
   IDCONTACT            int not null,
   primary key (IDCLIENT, IDCONTACT),
   constraint FK_CONTACTCLIENT foreign key (IDCLIENT)
      references Client (IDCLIENT) on delete restrict on update restrict,
   constraint FK_CONTACTCLIENT2 foreign key (IDCONTACT)
      references Contact (IDCONTACT) on delete restrict on update restrict
);

/*==============================================================*/
/* Table : Status                                               */
/*==============================================================*/
create table `Status`
(
   IDSTATUS             int not null auto_increment ,
   `LABEL`              varchar(50) not null,
   primary key (IDSTATUS)
);

/*==============================================================*/
/* Table : ContractType                                         */
/*==============================================================*/
create table ContractType
(
   IDCT                 char(3) not null UNIQUE,
   LABELCT              varchar(100) not null,
   primary key (IDCT)
);

/*==============================================================*/
/* Table : Contract                                             */
/*==============================================================*/
create table Contract
(
   IDCONTRACT           int not null auto_increment,
   IDSTATUS             int not null,
   IDCT                 char(3) not null,
   MATRICULE            int not null,
   STARTDATE            date not null,
   ENDDATE              date,
   SALARY               int not null,
   primary key (IDCONTRACT),
   constraint FK_STATUS foreign key (IDSTATUS)
      references Status (IDSTATUS) on delete restrict on update restrict,
   constraint FK_CONTRACTTYPE foreign key (IDCT)
      references ContractType (IDCT) on delete restrict on update restrict,
   constraint FK_SIGN foreign key (MATRICULE)
      references Collaborator (MATRICULE) on delete restrict on update restrict,
   check (startDate < endDate) 
);

/*==============================================================*/
/* Table : DefaultFunction                                      */
/*==============================================================*/
create table DefaultFunction
(
   MATRICULE            int not null,
   IDFUNCTIONTYPE       int not null,
   CHANGEDATE           date not null,
   primary key (MATRICULE, IDFUNCTIONTYPE),
   constraint FK_DEFAULTFUNCTION foreign key (MATRICULE)
      references Collaborator (MATRICULE) on delete restrict on update restrict,
   constraint FK_DEFAULTFUNCTION2 foreign key (IDFUNCTIONTYPE)
      references Functiontype (IDFUNCTIONTYPE) on delete restrict on update restrict
);

/*==============================================================*/
/* Table : Resource                                             */
/*==============================================================*/
create table `Resource`
(
   IDRESSOURCE          int not null auto_increment,
   MATRICULE            int not null,
   TITLE                varchar(50) not null,
   RESUME               varchar(255) not null,
   RELEASEDATE          DATE not null,
   LINK                 longblob,
   primary key (IDRESSOURCE),
   constraint FK_AUTHOR foreign key (MATRICULE)
      references Collaborator (MATRICULE) on delete restrict on update restrict
);

/*==============================================================*/
/* Table : ProjectLibrary                                       */
/*==============================================================*/
create table ProjectLibrary
(
   IDPROJECT            int not null,
   IDRESSOURCE          int not null,
   primary key (IDPROJECT, IDRESSOURCE),
   constraint FK_PROJECTLIBRARY foreign key (IDPROJECT)
      references Project (IDPROJECT) on delete restrict on update restrict,
   constraint FK_PROJECTLIBRARY2 foreign key (IDRESSOURCE)
      references Resource (IDRESSOURCE) on delete restrict on update restrict
);

/*==============================================================*/
/* Table : Technology                                */
/*==============================================================*/
create table Technology
(
   IDTECHNO             int not null auto_increment,
   `LABEL`              varchar(50),
   primary key (IDTECHNO)
);

/*==============================================================*/
/* Table : ProjectTechnology                                    */
/*==============================================================*/
create table ProjectTechnology
(
   IDTECHNO             int not null,
   IDPROJECT            int not null,
   primary key (IDTECHNO, IDPROJECT),
   constraint FK_PROJECTTECHNOLOGY foreign key (IDTECHNO)
      references Technology (IDTECHNO) on delete restrict on update restrict,
   constraint FK_PROJECTTECHNOLOGY2 foreign key (IDPROJECT)
      references Project (IDPROJECT) on delete restrict on update restrict
);



/*------------------------------------------------------------*/
