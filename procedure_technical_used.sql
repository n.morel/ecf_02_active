-- On souhaite obtenir sur un thème technique donné la liste des projets associés et terminés depuis moins de 2 ans

DROP PROCEDURE IF EXISTS technical_used;

DELIMITER $$

create Procedure technical_used(param varchar(50))
COMMENT "technologies used in project"
reads sql data
BEGIN
	declare techno_used varchar(50);
    set techno_used = param;
    
	select P.idproject from project P
	inner join projectTechnology PT 
	on P.idProject = PT.idproject
	inner join technology T
	on PT.IDTechno = T.IDTechno
	where datediff(curdate(),enddate) < 730
	and  label like techno_used;
     
     
END$$

DELIMITER ;

-- comment faire une fonction avec ca? que mettre en return?