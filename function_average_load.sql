-- On souhaite obtenir la moyenne des charges estimées sur les projets en cours.
DROP FUNCTION IF EXISTS average_load;

DELIMITER $$

create Function average_load()
returns integer
COMMENT "average load of in progress project "
reads sql data
BEGIN
	DECLARE nb_project, total_load INT DEFAULT 1;
    SELECT Count(*)
    from Project
    where endDate is null
    into nb_project;
     
    SELECT sum(loadEstimation)
    from Project P 
    inner join Step S
    on P.idProject = S.idProject
    where P.endDate is null
    into total_load;
    
    return total_load/nb_project;
   
END$$

DELIMITER ;


